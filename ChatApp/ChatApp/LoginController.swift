//
//  LoginController.swift
//  ChatApp
//
//  Created by Nabil Muthanna on 2016-10-21.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    // MARK: - UI
    private lazy var inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .whiteColor()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    private lazy var loginRegisterBtn: UIButton = {
        let btn = UIButton(type: .System)
        btn.setTitle("Register", forState: .Normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .orangeColor()
        btn.setTitleColor(.whiteColor(), forState: .Normal)
        btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        btn.layer.cornerRadius = 10
        btn.layer.masksToBounds = true
        return btn
    }()
    private lazy var nameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.tintColor = .orangeColor()
        textField.placeholder = "Name"
        return textField
    }()
    private lazy var nameSeperatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        return view
    }()
    private lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.tintColor = .orangeColor()
        textField.placeholder = "Email Address"
        return textField
    }()
    private lazy var emailSeperatorLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        return view
    }()
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Password"
        textField.secureTextEntry = true
        textField.tintColor = .orangeColor()
        return textField
    }()
    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "profle_placeholder")
        imageView.contentMode = .ScaleAspectFit
        return imageView
    }()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(r: 61, g: 91, b: 151)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewWillLayoutSubviews() {

        view.addSubview(inputsContainerView)
        view.addSubview(loginRegisterBtn)
        inputsContainerView.addSubview(nameTextField)
        inputsContainerView.addSubview(nameSeperatorLine)
        
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorLine)
        
        inputsContainerView.addSubview(passwordTextField)
        
        view.addSubview(profileImageView)
        
        
        NSLayoutConstraint.activateConstraints([
            
            profileImageView.topAnchor.constraintEqualToAnchor(view.topAnchor, constant: 32),
            profileImageView.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor),
            profileImageView.widthAnchor.constraintEqualToConstant(150),
            profileImageView.bottomAnchor.constraintEqualToAnchor(inputsContainerView.topAnchor, constant: -12),

            inputsContainerView.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor),
            inputsContainerView.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor),
            inputsContainerView.widthAnchor.constraintEqualToAnchor(view.widthAnchor, multiplier: 1.0, constant: -24),
            inputsContainerView.heightAnchor.constraintEqualToConstant(150),
            
            loginRegisterBtn.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor),
            loginRegisterBtn.heightAnchor.constraintEqualToConstant(50),
            loginRegisterBtn.topAnchor.constraintEqualToAnchor(inputsContainerView.bottomAnchor, constant: 8),
            loginRegisterBtn.widthAnchor.constraintEqualToAnchor(inputsContainerView.widthAnchor),
            
            nameTextField.topAnchor.constraintEqualToAnchor(inputsContainerView.topAnchor),
            nameTextField.leftAnchor.constraintEqualToAnchor(inputsContainerView.leftAnchor, constant: 12),
            nameTextField.widthAnchor.constraintEqualToAnchor(inputsContainerView.widthAnchor),
            nameTextField.heightAnchor.constraintEqualToAnchor(inputsContainerView.heightAnchor, multiplier: 1/3),
            
            nameSeperatorLine.topAnchor.constraintEqualToAnchor(nameTextField.bottomAnchor),
            nameSeperatorLine.widthAnchor.constraintEqualToAnchor(inputsContainerView.widthAnchor, multiplier: 1.0, constant: 12),
            nameSeperatorLine.heightAnchor.constraintEqualToConstant(1),
            
            emailTextField.topAnchor.constraintEqualToAnchor(nameTextField.bottomAnchor),
            emailTextField.widthAnchor.constraintEqualToAnchor(nameTextField.widthAnchor),
            emailTextField.leftAnchor.constraintEqualToAnchor(inputsContainerView.leftAnchor, constant: 12),
            emailTextField.heightAnchor.constraintEqualToAnchor(inputsContainerView.heightAnchor, multiplier: 1/3),

            emailSeperatorLine.topAnchor.constraintEqualToAnchor(emailTextField.bottomAnchor),
            emailSeperatorLine.widthAnchor.constraintEqualToAnchor(inputsContainerView.widthAnchor, multiplier: 1.0, constant: 12),
            emailSeperatorLine.heightAnchor.constraintEqualToConstant(1),
            
            passwordTextField.widthAnchor.constraintEqualToAnchor(nameTextField.widthAnchor),
            passwordTextField.heightAnchor.constraintEqualToAnchor(inputsContainerView.heightAnchor, multiplier: 1/3),
            passwordTextField.leftAnchor.constraintEqualToAnchor(inputsContainerView.leftAnchor, constant: 12),
            passwordTextField.bottomAnchor.constraintEqualToAnchor(inputsContainerView.bottomAnchor),

        ])
        
    }
}
