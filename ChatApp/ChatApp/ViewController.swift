//
//  ViewController.swift
//  ChatApp
//
//  Created by Nabil Muthanna on 2016-10-21.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = .redColor()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .Plain, target: self, action: #selector(ViewController.handleLogout))
        
    }

  
    
    @objc private func handleLogout() {
        let loginController = LoginController()
        
        presentViewController(loginController, animated: true, completion: nil)
    }

}

