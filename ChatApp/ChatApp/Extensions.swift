//
//  Extensions.swift
//  ChatApp
//
//  Created by Nabil Muthanna on 2016-10-21.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

