//
//  PhotoListController.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-23.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit
import CoreData

class PhotoListController: UIViewController {

    lazy var cameraButton: UIButton = {
        let btn = UIButton(type: .System)
        btn.setTitle("Camera", forState: .Normal)
        btn.tintColor = .whiteColor()
        btn.backgroundColor = UIColor.colorWithRgb(254, g: 123, b: 135)
        btn.addTarget(self, action: #selector(PhotoListController.presentImagePickerController), forControlEvents: .TouchUpInside)
        return btn
    }()
    
    lazy var mediaPickerManager: MediaPickerManager = {
        let manager = MediaPickerManager(presentingViewController: self)
        manager.delegate = self
        return manager
    }()
    
    lazy var dataSource: PhotoDataSource = {
        return PhotoDataSource(fetchRequest: Photo.allPhotosRequest, collectionView: self.collectionView)
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let paddingDistance: CGFloat = 16.0
        let itemSize = (screenWidth - paddingDistance)/2.0
        
        collectionViewLayout.itemSize = CGSize(width: itemSize, height: itemSize)
        
        let collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: collectionViewLayout)
        collectionView.backgroundColor = .whiteColor()
        collectionView.registerClass(PhotoCell.self, forCellWithReuseIdentifier: PhotoCell.reuseIdentifier)
        
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupNavigationBar()
        
        collectionView.dataSource = dataSource
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    // MARK: - Layout
    
    override func viewWillLayoutSubviews() {
        
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(cameraButton)
        cameraButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activateConstraints([
            
            collectionView.leftAnchor.constraintEqualToAnchor(view.leftAnchor),
            collectionView.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor),
            collectionView.rightAnchor.constraintEqualToAnchor(view.rightAnchor),
            collectionView.bottomAnchor.constraintEqualToAnchor(view.bottomAnchor),

            
            cameraButton.leftAnchor.constraintEqualToAnchor(view.leftAnchor),
            cameraButton.bottomAnchor.constraintEqualToAnchor(view.bottomAnchor),
            cameraButton.rightAnchor.constraintEqualToAnchor(view.rightAnchor),
            cameraButton.heightAnchor.constraintEqualToConstant(56.0),
            
        ])
    }
    
    
    
    // MARK: - Image Picker Controller

    @objc private func presentImagePickerController() {
       //pressed the camera btn
        mediaPickerManager.presentImagePickerController(animated: true)
    }
}



// MARK: - MediaPickerManagerDelegate

extension PhotoListController: MediaPickerManagerDelegate {
    func mediaPickerManager(manager: MediaPickerManager, didFinishPickingImage image: UIImage) {
        
        let eaglContext = EAGLContext(API: .OpenGLES2)
        let ciContext = CIContext(EAGLContext: eaglContext)
        let photoFilterController = PhotoFilterController(image: image, context: ciContext, eaglContext: eaglContext)
        let nv = UINavigationController(rootViewController: photoFilterController)
        
        mediaPickerManager.dismissImagePickerController(animated: true) { 
            self.presentViewController(nv, animated: true, completion: nil)
        }
    }
}

// Mark - Navigation

extension PhotoListController {
    
    func setupNavigationBar() {
        
        let sortTagsBtn = UIBarButtonItem(title: "Tags", style: .Plain, target: self, action: #selector(PhotoListController.presentTagsSortController))
        let sortLocationsBtn = UIBarButtonItem(title: "Locations", style: .Plain, target: self, action: #selector(PhotoListController.presentLocationsSortController))

        navigationItem.setRightBarButtonItems([sortTagsBtn, sortLocationsBtn], animated: true)
    }
    
    @objc private func presentTagsSortController() {
     
        let tagDataSource = SortableDataSource<Tag>(fetchedRequest: Tag.allTagsRequest, managedObjectContext: CoreDataController.sharedInstance.managedObjectContext)
        let sortItemSelector = SortItemSelector<Tag>(sortItems: tagDataSource.results)

        let sortController = PhotoSortListController(dataSource: tagDataSource, sortItemSelector: sortItemSelector)
        sortController.onSortSelection = { checkedItems in
            //construct a fetch request (compound predicate)
            
            if !checkedItems.isEmpty {
                var predicates = [NSPredicate]()
                for tag in checkedItems {
                    predicates.append(NSPredicate(format: "%K CONTAINS %@", "tags.title", tag.title))
                }
                let compoundPredicate = NSCompoundPredicate(type: .OrPredicateType, subpredicates: predicates)
                self.dataSource.performFetch(withPredicate: compoundPredicate)
            } else {
                self.dataSource.performFetch(withPredicate: nil)
            }
        }
        
        let nv = UINavigationController(rootViewController: sortController)
        presentViewController(nv, animated: true, completion: nil)
    }
    
    @objc private func presentLocationsSortController() {
        
        let tagDataSource = SortableDataSource<Tag>(fetchedRequest: Tag.allTagsRequest, managedObjectContext: CoreDataController.sharedInstance.managedObjectContext)
        let sortItemSelector = SortItemSelector<Tag>(sortItems: tagDataSource.results)
        
        let sortController = PhotoSortListController(dataSource: tagDataSource, sortItemSelector: sortItemSelector)
        
        let nv = UINavigationController(rootViewController: sortController)
        presentViewController(nv, animated: true, completion: nil)
    }
}



