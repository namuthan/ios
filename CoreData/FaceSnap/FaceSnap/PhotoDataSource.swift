//
//  PhotoDataSource.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-29.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class PhotoDataSource: NSObject {
    
    
    private let collectionView: UICollectionView
    private let managedObjectContext = CoreDataController.sharedInstance.managedObjectContext
    private let fetchedResultController: PhotoFetechedResultsController
    
    init(fetchRequest: NSFetchRequest, collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.fetchedResultController = PhotoFetechedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, collectionView: self.collectionView)
        
        super.init()
    }
    
    func performFetch(withPredicate predicate: NSPredicate?) {
        self.fetchedResultController.performFetch(withPredicate: predicate)
        collectionView.reloadData()
    }
    
}

extension PhotoDataSource: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return fetchedResultController.sections?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let section = fetchedResultController.sections?[section] else { return 0 }
        return section.numberOfObjects
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoCell.reuseIdentifier, forIndexPath: indexPath) as! PhotoCell
        let photo = fetchedResultController.objectAtIndexPath(indexPath) as! Photo
        cell.imageView.image = photo.photoImage
        
        return cell
    }
    
}
