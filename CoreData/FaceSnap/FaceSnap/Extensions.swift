//
//  File.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-23.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


extension UIColor {
    
    static func colorWithRgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    var red: CGFloat{
        return CGColorGetComponents(self.CGColor)[0]
    }
    
    var green: CGFloat{
        return CGColorGetComponents(self.CGColor)[1]
    }
    
    var blue: CGFloat{
        return CGColorGetComponents(self.CGColor)[2]
    }
    
    var alpha: CGFloat{
        return CGColorGetComponents(self.CGColor)[3]
    }
}
