//
//  FilteredImageBuilder.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-23.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import CoreImage
import UIKit

protocol FilterInit {
    var filter: CIFilter? { get }
}



//take an input image, apply a filter and output a recipe of the how the output image should be rendered

final class FilteredImageBuilder {
    
    private struct PhotoFilter {
        
        private enum PhotoFilterType: FilterInit {
            
            private struct PhotoFiltersNameKey {
                static let ColorClamp = "CIColorClamp"
                static let ColorControls = "CIColorControls"
                static let PhotoEffectInstant = "CIPhotoEffectInstant"
                static let PhotoEffectProcess = "CIPhotoEffectProcess"
                static let PhotoEffectNoir  = "CIPhotoEffectNoir"
                static let Sepia = "CISepiaTone"
                
                static let BumpDistortion = "CIBumpDistortion"
                static let CIGaussianBlur = "CIGaussianBlur"
                static let CIPixellate = "CIPixellate"
                static let CITwirlDistortion = "CITwirlDistortion"
                static let CIUnsharpMask = "CIUnsharpMask"
                static let CIVignette = "CIVignette"

            }
            
            case ColorClamp(minComponent: UIColor, maxComponent: UIColor)
            case ColorControls(saturation: NSNumber, brightness: NSNumber, contrast: NSNumber)
            case PhotoEffectInstant
            case PhotoEffectProcess
            case PhotoEffectNoir
            case SepiaTone(intensity: NSNumber)
            case BumpDistortion(inputCenter: CIVector, inputRadius: NSNumber, inputScale: NSNumber)
            
            var filter: CIFilter? {
                switch self {
                case .ColorClamp(let minComponent, let maxComponent):
                    let filter = CIFilter(name: PhotoFiltersNameKey.ColorClamp)
                    filter?.setValue(CIVector(x: minComponent.red, y: minComponent.green, z: minComponent.blue, w: minComponent.alpha), forKey: "inputMinComponents")
                    filter?.setValue(CIVector(x: maxComponent.red, y: maxComponent.green, z: maxComponent.blue, w: maxComponent.alpha), forKey: "inputMaxComponents")
                    return filter
                case .ColorControls(let saturation, let brightness, let contrast):
                    let filter = CIFilter(name: PhotoFiltersNameKey.ColorControls)
                    filter?.setValue(saturation, forKey: kCIInputSaturationKey)
                    filter?.setValue(brightness, forKey: kCIInputBrightnessKey)
                    filter?.setValue(contrast, forKey: kCIInputContrastKey)
                    return filter
                case .PhotoEffectInstant:
                    let filter = CIFilter(name: PhotoFiltersNameKey.PhotoEffectInstant)
                    return filter
                case .PhotoEffectProcess:
                    let filter = CIFilter(name: PhotoFiltersNameKey.PhotoEffectProcess)
                    return filter
                case .PhotoEffectNoir:
                    let filter = CIFilter(name: PhotoFiltersNameKey.PhotoEffectNoir)
                    return filter
                case .SepiaTone(let intensity):
                    let filter = CIFilter(name: PhotoFiltersNameKey.Sepia)
                    filter?.setValue(intensity, forKey: kCIInputIntensityKey)
                    return filter
                case .BumpDistortion(let inputCenter, let inputRadius, let inputScale):
                    let filter = CIFilter(name: PhotoFiltersNameKey.BumpDistortion)
                    filter?.setValue(inputCenter, forKey: kCIInputCenterKey)
                    filter?.setValue(inputRadius, forKey: kCIInputRadiusKey)
                    filter?.setValue(inputScale, forKey: kCIInputScaleKey)
                    return filter
                }
            }
        }
        
        static func defaultFilters(image: UIImage) -> [CIFilter] {
            return [
                PhotoFilterType.ColorClamp(minComponent: UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.2),
                    maxComponent: UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.9)).filter!,
                PhotoFilterType.ColorControls(saturation: 0.1, brightness: 0.1, contrast: 0.1).filter!,
                PhotoFilterType.PhotoEffectInstant.filter!,
                PhotoFilterType.PhotoEffectProcess.filter!,
                PhotoFilterType.PhotoEffectNoir.filter!,
                PhotoFilterType.SepiaTone(intensity: 0.7).filter!,
                
                PhotoFilterType.BumpDistortion(inputCenter: CIVector(x: image.size.width / 2, y: image.size.height / 2), inputRadius: (image.size.height < image.size.width ? image.size.height : image.size.width) / 2 , inputScale: 2).filter!
            ]
        }
    }
    
    private let image: UIImage
    private let context: CIContext
    
    init(image: UIImage, context: CIContext) {
        self.image = image
        self.context = context
    }
    
    func imageWithDefaultFilters() -> [CIImage] {
        return image(withFilters: PhotoFilter.defaultFilters(self.image))
    }
    
    func image(withFilters filters:[CIFilter])-> [CIImage] {
        return filters.map {
            image(self.image, withFilter: $0)
        }
    }
    
    func image(image: UIImage, withFilter filter: CIFilter) -> CIImage {
        let inputImage = image.CIImage ?? CIImage(image: image)!
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        return  filter.outputImage!.imageByCroppingToRect(inputImage.extent)
    }
    
}



