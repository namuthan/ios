//
//  PhotoMetadataController.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-27.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit
import CoreLocation

class PhotoMetadataController: UITableViewController {
    
    // MARK: Metadata fields
    
    private let photo : UIImage
    
    init(photo: UIImage) {
        self.photo = photo
        super.init(style: .Grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var photoImageView: UIImageView = {
        let view = UIImageView(image: self.photo)
        view.contentMode = .ScaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imageViewHeight: CGFloat = {
        let imgFactor = self.photoImageView.frame.size.height / self.photoImageView.frame.size.width
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        return screenWidth * imgFactor
    }()
    private lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.text = "Tap to add location"
        label.textColor = UIColor.lightGrayColor()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidden = true
        return view
    }()
    private var locationManager: LocationManager!
    private var location: CLLocation?
    
    private lazy var tagInputTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Type Tag Here!"
        return textField
    }()
    
    private lazy var addTagBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Add Tag", forState: .Normal)
        btn.setTitleColor(.redColor(), forState: .Normal)
        return btn
    }()
    
    private lazy var tagsView: PhotoTagListView = {
        let tagsView = PhotoTagListView()
        tagsView.translatesAutoresizingMaskIntoConstraints = false
        return tagsView
    }()
    
    var photoTags = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: #selector(PhotoMetadataController.savePhotoWithMetaData))
        navigationItem.rightBarButtonItem = saveBtn
    }
    
    @objc private func addNewTag() {
        guard let textViewText = tagInputTextField.text else { return }
        guard !textViewText.isEmpty else { return }
        
        photoTags.append(textViewText)
        tagsView.addNetTag(textViewText)
    }
    
    @objc private func savePhotoWithMetaData() {

        Photo.photoWith(photo, tags: tagsView.readTags(), location: location)
    
        CoreDataController.save()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tagsForPhoto() -> [String] {
        return tagsView.readTags()
    }
}



extension PhotoMetadataController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.selectionStyle = .None
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell.contentView.addSubview(photoImageView)
            NSLayoutConstraint.activateConstraints([
                
                photoImageView.topAnchor.constraintEqualToAnchor(cell.contentView.topAnchor),
                photoImageView.rightAnchor.constraintEqualToAnchor(cell.contentView.rightAnchor),
                photoImageView.bottomAnchor.constraintEqualToAnchor(cell.contentView.bottomAnchor),
                photoImageView.leftAnchor.constraintEqualToAnchor(cell.contentView.leftAnchor)
                
            ])
        case (1, 0):
            cell.contentView.addSubview(locationLabel)
            cell.contentView.addSubview(activityIndicator)
            
            NSLayoutConstraint.activateConstraints([
               
                activityIndicator.centerYAnchor.constraintEqualToAnchor(cell.contentView.centerYAnchor),
                activityIndicator.leftAnchor.constraintEqualToAnchor(cell.contentView.leftAnchor, constant: 20.0),

                locationLabel.topAnchor.constraintEqualToAnchor(cell.contentView.topAnchor),
                locationLabel.rightAnchor.constraintEqualToAnchor(cell.contentView.rightAnchor, constant: 16.0),
                locationLabel.bottomAnchor.constraintEqualToAnchor(cell.contentView.bottomAnchor),
                locationLabel.leftAnchor.constraintEqualToAnchor(cell.contentView.leftAnchor, constant: 20)
                
            ])

        case (2, 0):
            cell.contentView.addSubview(tagInputTextField)
            cell.contentView.addSubview(addTagBtn)
            cell.contentView.addSubview(tagsView)
            addTagBtn.addTarget(self, action: #selector(PhotoMetadataController.addNewTag), forControlEvents: .TouchUpInside)
            NSLayoutConstraint.activateConstraints([
                
                tagInputTextField.leftAnchor.constraintEqualToAnchor(cell.contentView.leftAnchor, constant: 20),
                tagInputTextField.topAnchor.constraintEqualToAnchor(cell.contentView.topAnchor, constant: 8),
                tagInputTextField.heightAnchor.constraintEqualToConstant(50),
                tagInputTextField.rightAnchor.constraintEqualToAnchor(addTagBtn.leftAnchor, constant: 8),
                
                addTagBtn.rightAnchor.constraintEqualToAnchor(cell.rightAnchor, constant: -8),
                addTagBtn.centerYAnchor.constraintEqualToAnchor(tagInputTextField.centerYAnchor),
                addTagBtn.widthAnchor.constraintEqualToConstant(100),
                
                tagsView.topAnchor.constraintEqualToAnchor(tagInputTextField.bottomAnchor, constant: 8),
                tagsView.rightAnchor.constraintEqualToAnchor(cell.contentView.rightAnchor, constant: -20),
                tagsView.bottomAnchor.constraintEqualToAnchor(cell.contentView.bottomAnchor),
                tagsView.leftAnchor.constraintEqualToAnchor(cell.contentView.leftAnchor, constant: 20)
                
            ])
            
        default:
            break
        }
        
        return cell
    }
    
}



extension PhotoMetadataController {
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Photo"
        case 1:
            return "Enter a location"
        case 2:
            return "Enter tags"
        default:
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            return imageViewHeight
        case (2, 0):
            return 300
        default:
           return tableView.rowHeight
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (1, 0):
            locationLabel.hidden = true
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            locationManager = LocationManager()
            locationManager.onLocationFix = { placemark, error in
                
                if let placemark = placemark {
                    self.location = placemark.location!
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    self.locationLabel.hidden = false
                    
                    guard let name = placemark.name, city = placemark.locality, area = placemark.administrativeArea else { return }
                    
                    self.locationLabel.text = "\(name), \(city), \(area)"
                }
            }
        default:
            break
        }
    }
}


