//
//  PhotoTagCell.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-27.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

class PhotoTagCell: UICollectionViewCell {
    static let reuseIdentifier = String(PhotoTagCell.self)

    lazy var tagLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "tag for photo"
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    lazy var deleteBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "x_icon.png")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        btn.tintColor = .redColor()
        return btn
    }()
    
    override func layoutSubviews() {
        
        contentView.layer.cornerRadius = 5
        contentView.backgroundColor = UIColor.darkGrayColor()
        
        contentView.addSubview(tagLabel)
        contentView.addSubview(deleteBtn)
        
        NSLayoutConstraint.activateConstraints([
            tagLabel.leftAnchor.constraintEqualToAnchor(contentView.leftAnchor, constant: 8),
            tagLabel.centerYAnchor.constraintEqualToAnchor(contentView.centerYAnchor),
            tagLabel.rightAnchor.constraintEqualToAnchor(deleteBtn.leftAnchor, constant: 8),
            
            deleteBtn.centerYAnchor.constraintEqualToAnchor(contentView.centerYAnchor),
            deleteBtn.rightAnchor.constraintEqualToAnchor(contentView.rightAnchor),
            deleteBtn.widthAnchor.constraintEqualToConstant(30)
        ])
    }
}

