//
//  PhotoCell.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-29.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    
    static let reuseIdentifier = "\(PhotoCell.self)"
    
    let imageView = UIImageView()
    
    
    override func layoutSubviews() {
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activateConstraints([
            
            imageView.leftAnchor.constraintEqualToAnchor(contentView.leftAnchor),
            imageView.topAnchor.constraintEqualToAnchor(contentView.topAnchor),
            imageView.rightAnchor.constraintEqualToAnchor(contentView.rightAnchor),
            imageView.bottomAnchor.constraintEqualToAnchor(contentView.bottomAnchor),

        ])
    }

}
