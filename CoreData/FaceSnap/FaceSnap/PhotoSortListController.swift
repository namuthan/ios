//
//  PhotoSortListController.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-29.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit
import CoreData

class PhotoSortListController<SortType: CustomTitleConvertable where SortType: NSManagedObject>: UITableViewController {
    
    let dataSource: SortableDataSource<SortType>
    let sortItemSelector: SortItemSelector<SortType>

    var onSortSelection: (Set<SortType> -> Void)?
    
    init(dataSource: SortableDataSource<SortType>, sortItemSelector: SortItemSelector<SortType>) {
        self.dataSource = dataSource
        self.sortItemSelector = sortItemSelector

        super.init(style: .Grouped)
        tableView.dataSource = dataSource
        tableView.delegate = sortItemSelector
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigation()
    }
    
    private func setupNavigation() {
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(PhotoSortListController.dismissPhotoSortListController))
        navigationItem.rightBarButtonItem = doneBtn
    }
    
    @objc private func dismissPhotoSortListController() {        
        guard let onSortSelection = onSortSelection else { return }
        onSortSelection(sortItemSelector.checkedItems)
        dismissViewControllerAnimated(true, completion: nil)
    }

}
