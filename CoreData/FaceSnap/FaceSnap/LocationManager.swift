//
//  LocationManager.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-27.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject {

    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var onLocationFix: ((CLPlacemark?, NSError?) -> Void)?
    
    override init() {
        super.init()
        manager.delegate = self
        getPermission()
    }
    
    
    private func getPermission() {
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            manager.requestWhenInUseAuthorization()
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            manager.requestLocation()
        }
        //TODO - Handle other paths(user refueses to give access, ...)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Unresolved error with location manager \(error), \(error.userInfo)")
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        
        //convert coordinates to addresses with geolocation
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            if let onLocationFix = self.onLocationFix {
                onLocationFix(placemarks?.first, error)
            }
        }
    }

}