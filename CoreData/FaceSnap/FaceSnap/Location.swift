//
//  Location.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-29.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class Location: NSManagedObject {
    static let entityName = "\(Location.self)"
    
    
    static var allTagsRequest: NSFetchRequest = {
        let request = NSFetchRequest(entityName: Location.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "latitude", ascending: true)]
        return request
    }()
    
    class func location(withLatitude latitude: Double, andLongitude longitude: Double) -> Location {
        let location = NSEntityDescription.insertNewObjectForEntityForName(Location.entityName, inManagedObjectContext: CoreDataController.sharedInstance.managedObjectContext) as! Location
        location.latitude = latitude
        location.longitude = longitude

        return location
    }
    
}

extension Location {
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    
    var title: String {
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            return placemarks?.first
        }
        return "\(latitude) -- \(longitude)"
    }
}