//
//  PhotoTagListView.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-27.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


class PhotoTagListView: UIView {
    
    private var photoTags: [String] = [String]()
    
    lazy var tagsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        layout.itemSize = CGSize(width: 100, height: 50)
        let collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        collectionView.backgroundColor = .whiteColor()
        collectionView.registerClass(PhotoTagCell.self, forCellWithReuseIdentifier: PhotoTagCell.reuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView

    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addSubview(tagsCollectionView)
        
        NSLayoutConstraint.activateConstraints([
            
            tagsCollectionView.leftAnchor.constraintEqualToAnchor(leftAnchor),
            tagsCollectionView.rightAnchor.constraintEqualToAnchor(rightAnchor),
            tagsCollectionView.topAnchor.constraintEqualToAnchor(topAnchor),
            tagsCollectionView.bottomAnchor.constraintEqualToAnchor(bottomAnchor)
            
        ])
    }
}

extension PhotoTagListView {
    
    func addNetTag(tag: String) {
        photoTags.append(tag.lowercaseString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()))
        let indexPath = NSIndexPath(forItem: photoTags.count - 1, inSection: 0)
        
        UIView.animateWithDuration(0.2, delay: 0.01, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .CurveEaseOut, animations: {
          
            self.tagsCollectionView.insertItemsAtIndexPaths([indexPath])
            self.tagsCollectionView.reloadItemsAtIndexPaths([indexPath])
            self.tagsCollectionView.setNeedsLayout()

        }, completion: nil)
        
    }
    
    func readTags() -> [String] {
        return self.photoTags
    }
}



extension PhotoTagListView: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoTags.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoTagCell.reuseIdentifier, forIndexPath: indexPath) as! PhotoTagCell
        cell.tagLabel.text = photoTags[indexPath.row] ?? ""
        return cell
    }
}

extension PhotoTagListView: UICollectionViewDelegate {
    
}