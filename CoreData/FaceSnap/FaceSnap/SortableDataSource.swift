//
//  SortableDataSource.swift
//  FaceSnap
//
//  Created by Nabil Muthanna on 2016-09-29.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol CustomTitleConvertable {
    var title: String { get }
}


extension Tag: CustomTitleConvertable {}
extension Location: CustomTitleConvertable {}

class SortableDataSource<SortType: CustomTitleConvertable where SortType: NSManagedObject>: NSObject, UITableViewDataSource {
    
    private let fetechResultController: NSFetchedResultsController
    var results: [SortType] {
        return fetechResultController.fetchedObjects as! [SortType]
    }
    
    init(fetchedRequest: NSFetchRequest, managedObjectContext: NSManagedObjectContext) {
        self.fetechResultController = NSFetchedResultsController(fetchRequest: fetchedRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        super.init()
        executeFetch()
    }
    
    private func executeFetch() {
        do {
            try fetechResultController.performFetch()
        } catch let error as NSError {
            print("unresolved error \(error) \(error.userInfo)")
        }
    }
    
    
    // MARK : UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return fetechResultController.fetchedObjects?.count ?? 0
        default:
            return 0
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .Default, reuseIdentifier: "sortableItemCell")
        cell.selectionStyle = .None
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell.textLabel?.text = "All \(SortType.self)"
            cell.accessoryType = .Checkmark
        case (1, _):
            guard let sortItem = fetechResultController.fetchedObjects?[indexPath.row] as? SortType else { break }
            cell.textLabel?.text = sortItem.title
        default:
            break
        }
        
        return cell
    }
    
}