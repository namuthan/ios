//
//  NSDateExtension.swift
//  CustomCalenderView
//
//  Created by Nabil Muthanna on 2016-10-07.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import Foundation



extension NSDate {
    
    func dayOfTheWeek() -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.stringFromDate(self)
    }
    
    func monthOdTheYear() -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM"
        return dateFormatter.stringFromDate(self)
    }
    
    func dayNumberOfTheMonth() -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd"
        return dateFormatter.stringFromDate(self)
    }
    
    func year() -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.stringFromDate(self)
    }
    
    
    func monthAndYear() -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.stringFromDate(self)
    }
    
    func dayOfTheWeekEnum() -> WeekDay? {
        let day: String = self.dayOfTheWeek() ?? ""
        switch day {
        case "Sunday":
            return WeekDay.Sunday
        case "Monday":
            return WeekDay.Monday
        case "Tuesday":
            return WeekDay.Tuesday
        case "Wednesday":
            return WeekDay.Wednesday
        case "Thursday":
            return WeekDay.Thuresday
        case "Friday":
            return WeekDay.Friday
        case "Saturday":
            return WeekDay.Saturday
        default:
            return nil
        }
    }
    
    func dayOfTheWeekInt(dayIndex: Int) -> WeekDay? {
        switch dayIndex {
        case 0:
            return WeekDay.Sunday
        case 1:
            return WeekDay.Monday
        case 2:
            return WeekDay.Tuesday
        case 3:
            return WeekDay.Wednesday
        case 4:
            return WeekDay.Thuresday
        case 5:
            return WeekDay.Friday
        case 6:
            return WeekDay.Saturday
        default:
            return nil
        }
    }
    
    func startOfMonth() -> NSDate? {
        guard
            let cal: NSCalendar = NSCalendar.currentCalendar(),
            let comp: NSDateComponents = cal.components([.Year, .Month], fromDate: self) else { return nil }
        comp.to12pm()
        return cal.dateFromComponents(comp)!
    }
    
    func endOfMonth() -> NSDate? {
        guard
            let cal: NSCalendar = NSCalendar.currentCalendar(),
            let comp: NSDateComponents = NSDateComponents() else { return nil }
        comp.month = 1
        comp.day = -1
        comp.to12pm()
        return cal.dateByAddingComponents(comp, toDate: self.startOfMonth()!, options: [])!
    }
    
    func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    
    func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    
    func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    
    func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    
    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    
    func minutesFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    
    func secondsFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
}

internal extension NSDateComponents {
    func to12pm() {
        self.hour = 12
        self.minute = 0
        self.second = 0
    }
}

enum WeekDay {
    
    case Sunday
    case Monday
    case Tuesday
    case Wednesday
    case Thuresday
    case Friday
    case Saturday
    
}