//
//  CalendarView.swift
//  CustomCalenderView
//
//  Created by Nabil Muthanna on 2016-10-07.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


class CalendarView: UIView {
    
    var startDate: NSDate = NSDate() {
        didSet {
            endDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: 10, toDate: startDate, options: NSCalendarOptions.init(rawValue: 0))!
            currentDisplayedDate = NSDate()
        }
    }
    var endDate: NSDate = NSDate() {
        didSet {
            
        }
    }
    var currentDisplayedDate: NSDate = NSDate() {
        didSet {
            
        }
    }
    
    func scrollToCurrentMonth() {
        let months = currentDisplayedDate.monthsFrom(startDate)
        monthsCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: months, inSection: 0), atScrollPosition: .None, animated: false)
    }
    
    func invalidateLayout() {
        monthsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    private var displayedCell: Int = 0
    
    // views
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var headerView: CalenderHeaderView = {
        let view = CalenderHeaderView()
        view.backgroundColor = AppStyle.MANIN_COLOR
        view.translatesAutoresizingMaskIntoConstraints = false
        view.date = NSDate()
        view.headerViewTextColor = UIColor.whiteColor()
        return view
    }()
    
    private lazy var monthsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.pagingEnabled = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(MonthCell.self, forCellWithReuseIdentifier: MonthCell.reuseIdentifier)
        collectionView.clipsToBounds = true
        collectionView.backgroundColor = .whiteColor()

        return collectionView
    }()
    
    override func layoutSubviews() {
        
        addSubview(headerView)
        addSubview(monthsCollectionView)
        
        NSLayoutConstraint.activateConstraints([

            headerView.leftAnchor.constraintEqualToAnchor(leftAnchor),
            headerView.rightAnchor.constraintEqualToAnchor(rightAnchor),
            headerView.topAnchor.constraintEqualToAnchor(topAnchor),
            headerView.heightAnchor.constraintEqualToConstant(160),
            
            monthsCollectionView.topAnchor.constraintEqualToAnchor(headerView.bottomAnchor, constant: 8),
            monthsCollectionView.leftAnchor.constraintEqualToAnchor(leftAnchor),
            monthsCollectionView.rightAnchor.constraintEqualToAnchor(rightAnchor),
            monthsCollectionView.bottomAnchor.constraintEqualToAnchor(bottomAnchor),

        ])
        
        monthsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
}


extension CalendarView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return endDate.monthsFrom(startDate)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MonthCell.reuseIdentifier, forIndexPath: indexPath) as! MonthCell
        cell.monthDate = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Month, value: indexPath.item, toDate: startDate, options: NSCalendarOptions.init(rawValue: 0))!
        displayedCell = indexPath.item
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }

}




class MonthCell: UICollectionViewCell {
    
    static let reuseIdentifier = "\(MonthCell.self)"
    
    var monthDate: NSDate = NSDate() {
        didSet {
            monthYearLabel.text = monthDate.monthAndYear()
            let calendar = NSCalendar.currentCalendar()
            calendar.rangeOfUnit(.Month, startDate: &startOfMonth, interval: &lengthOfMonth, forDate: monthDate)
            numberOfDaysInMonth = Int(lengthOfMonth / dayIntervalInSeconds)
            if let startOfMonth = startOfMonth {
                numberOfEmptyCells = getNumberOfEmptyCells(startOfMonth)
            }
            
            print("start day of month \(monthDate.monthAndYear()) is - \(startOfMonth?.dayOfTheWeek())")
        }
    }
    
    private var startOfMonth: NSDate?
    private var lengthOfMonth : NSTimeInterval = 0
    private var numberOfDaysInMonth = 0
    private let dayIntervalInSeconds: Double = 86400
    private var numberOfEmptyCells = 0
    
    private lazy var monthYearLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(25)
        label.textAlignment = .Center
        label.sizeToFit()
        label.textColor = .blackColor()
        return label
    }()
    
    private lazy var daysCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.pagingEnabled = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(CalenderDayHeaderCell.self, forCellWithReuseIdentifier: CalenderDayHeaderCell.reuseIdentifier)
        collectionView.registerClass(CalenderDayCell.self, forCellWithReuseIdentifier: CalenderDayCell.reuseIdentifier)
        collectionView.registerClass(CalenderDayEmptyCell.self, forCellWithReuseIdentifier: CalenderDayEmptyCell.reuseIdentifier)
        collectionView.clipsToBounds = true
        collectionView.backgroundColor = .whiteColor()
        return collectionView
    }()
    
    override func prepareForReuse() {
        daysCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func layoutSubviews() {
     
        addSubview(monthYearLabel)
        addSubview(daysCollectionView)
        
        backgroundColor = .whiteColor()

        NSLayoutConstraint.activateConstraints([
            
            monthYearLabel.topAnchor.constraintEqualToAnchor(topAnchor),
            monthYearLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            
            daysCollectionView.topAnchor.constraintEqualToAnchor(monthYearLabel.bottomAnchor, constant: 8),
            daysCollectionView.leftAnchor.constraintEqualToAnchor(leftAnchor),
            daysCollectionView.rightAnchor.constraintEqualToAnchor(rightAnchor),
            daysCollectionView.bottomAnchor.constraintEqualToAnchor(bottomAnchor),
            
        ])
        
        daysCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    private func getNumberOfEmptyCells(startDate: NSDate) -> Int {
        
        if let dayOfTheWeek = startDate.dayOfTheWeekEnum() {
            
            switch dayOfTheWeek {
            case .Sunday:
                return 0
            case .Monday:
                return 1
            case .Tuesday:
                return 2
            case .Wednesday:
                return 3
            case .Thuresday:
                return 4
            case .Friday:
                return 5
            case .Saturday:
                return 6
            }
        }
        
        return 0
    }
}


extension MonthCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 7
        case 1:
            return numberOfEmptyCells  + numberOfDaysInMonth
        default:
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CalenderDayHeaderCell.reuseIdentifier, forIndexPath: indexPath) as! CalenderDayHeaderCell
            cell.dayHeader = getDayForSection(indexPath.row)
            cell.backgroundColor = .whiteColor()
            return cell
            
        case 1:
            
            if indexPath.item < numberOfEmptyCells {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CalenderDayEmptyCell.reuseIdentifier, forIndexPath: indexPath) as! CalenderDayEmptyCell
                cell.backgroundColor = .whiteColor()
                return cell
            } else {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CalenderDayCell.reuseIdentifier, forIndexPath: indexPath) as! CalenderDayCell
                cell.day = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Day, value: indexPath.item - numberOfEmptyCells, toDate: startOfMonth!, options: NSCalendarOptions.init(rawValue: 0))
                cell.backgroundColor = .whiteColor()
                return cell
            }
            
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CalenderDayEmptyCell.reuseIdentifier, forIndexPath: indexPath) as! CalenderDayEmptyCell
            cell.backgroundColor = .whiteColor()
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 7, height: collectionView.frame.height / 7)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
  
    
    private func getDayForSection(index: Int) -> String? {
        switch index {
        case 0:
            return "Sun"
        case 1:
            return "Mon"
        case 2:
            return "Tue"
        case 3:
            return "Wed"
        case 4:
            return "Thu"
        case 5:
            return "Fri"
        case 6:
            return "Sat"
        default:
            return nil
        }
    }
}






class CalenderDayEmptyCell: UICollectionViewCell {
    static let reuseIdentifier = "\(CalenderDayEmptyCell.self)"
}

class CalenderDayHeaderCell: UICollectionViewCell {
    static let reuseIdentifier = "\(CalenderDayHeaderCell.self)"

    var dayHeader: String? {
        didSet {
            dayHeaderLabel.text = dayHeader
        }
    }
    
    private lazy var dayHeaderLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(16)
        label.textAlignment = .Center
        label.sizeToFit()
        return label
    }()
    
    override func layoutSubviews() {
        
        addSubview(dayHeaderLabel)

        NSLayoutConstraint.activateConstraints([
            
            dayHeaderLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            dayHeaderLabel.centerYAnchor.constraintEqualToAnchor(centerYAnchor),
        ])

    }
}

class CalenderDayCell: UICollectionViewCell {
    static let reuseIdentifier = "\(CalenderDayCell.self)"
    
    var day: NSDate? {
        didSet {
            dayLabel.text = "\(day?.dayNumberOfTheMonth() ?? "")"
        }
    }
    
    private lazy var dayLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.font = UIFont.systemFontOfSize(16)
        label.textAlignment = .Center
        label.sizeToFit()
        return label
    }()
    private lazy var ringView: Ring = {
        let view = Ring()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clearColor()
        return view
    }()
    
    override var selected: Bool {
        didSet {
            if self.selected {
                ringView.fillColor = AppStyle.MANIN_COLOR
                ringView.strokeColor = .clearColor()
                dayLabel.textColor = .whiteColor()
                
            } else {
                ringView.fillColor = .whiteColor()
                ringView.strokeColor = AppStyle.MANIN_COLOR
                dayLabel.textColor = .blackColor()
            }
        }
    }

    override func layoutSubviews() {
        
        //contentView.
        addSubview(ringView)
        addSubview(dayLabel)

        NSLayoutConstraint.activateConstraints([

            
            ringView.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            ringView.centerYAnchor.constraintEqualToAnchor(centerYAnchor),
            
            ringView.widthAnchor.constraintEqualToConstant(50),
            ringView.heightAnchor.constraintEqualToConstant(50),

            dayLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            dayLabel.centerYAnchor.constraintEqualToAnchor(centerYAnchor),
        ])
        
    }
}



