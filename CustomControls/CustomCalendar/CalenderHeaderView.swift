//
//  CalenderHeaderView.swift
//  CustomCalenderView
//
//  Created by Nabil Muthanna on 2016-10-07.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit


class CalenderHeaderView: UIView {
    
    var headerViewTextColor: UIColor = UIColor.whiteColor() {
        didSet {
            dayLabel.textColor = headerViewTextColor
            monthLabel.textColor = headerViewTextColor
            dateLabel.textColor = headerViewTextColor
            yearLabel.textColor = headerViewTextColor
        }
    }
    var date: NSDate = NSDate() {
        didSet {
            dayLabel.text = date.dayOfTheWeek()
            monthLabel.text = date.monthOdTheYear()?.uppercaseString
            dateLabel.text = date.dayNumberOfTheMonth()
            yearLabel.text = date.year()
        }
    }
    var yearLabelIsClicked: (() -> Void)?
    
    private lazy var dayLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFontOfSize(100)
        label.textAlignment = .Center
        label.sizeToFit()
        return label
    }()
    private lazy var monthLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .Center
        label.font = UIFont.boldSystemFontOfSize(100)
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.boldSystemFontOfSize(100)
        label.numberOfLines = 0
        label.textAlignment = .Center
        return label
    }()
    private lazy var yearLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.boldSystemFontOfSize(100)
        label.numberOfLines = 0
        label.textAlignment = .Center
        label.userInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CalenderHeaderView.yearLabelIsClicked(_:)))
        label.addGestureRecognizer(tapGesture)
        return label
    }()
    
    @objc private func yearLabelIsClicked(sender: AnyObject) {
        if let yearLabelIsClicked = yearLabelIsClicked {
            yearLabelIsClicked()
        }
    }
    
    override func layoutSubviews() {
        setupViews()
    }
    
    private func setupViews() {
        
        addSubview(dayLabel)
        addSubview(monthLabel)
        addSubview(dateLabel)
        addSubview(yearLabel)
        
        NSLayoutConstraint.activateConstraints([
            
            
            dayLabel.topAnchor.constraintEqualToAnchor(topAnchor),
            dayLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            dayLabel.heightAnchor.constraintEqualToAnchor(heightAnchor, multiplier: 0.15),
            dayLabel.leftAnchor.constraintEqualToAnchor(leftAnchor),
            dayLabel.rightAnchor.constraintEqualToAnchor(rightAnchor),

            monthLabel.topAnchor.constraintEqualToAnchor(dayLabel.bottomAnchor),
            monthLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            monthLabel.heightAnchor.constraintEqualToAnchor(heightAnchor, multiplier: 0.20),
            monthLabel.leftAnchor.constraintEqualToAnchor(leftAnchor),
            monthLabel.rightAnchor.constraintEqualToAnchor(rightAnchor),

            dateLabel.topAnchor.constraintEqualToAnchor(monthLabel.bottomAnchor),
            dateLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            dateLabel.heightAnchor.constraintEqualToAnchor(heightAnchor, multiplier: 0.45),
            dateLabel.leftAnchor.constraintEqualToAnchor(leftAnchor),
            dateLabel.rightAnchor.constraintEqualToAnchor(rightAnchor),

            yearLabel.bottomAnchor.constraintEqualToAnchor(bottomAnchor),
            yearLabel.centerXAnchor.constraintEqualToAnchor(centerXAnchor),
            yearLabel.heightAnchor.constraintEqualToAnchor(heightAnchor, multiplier: 0.20),
            yearLabel.leftAnchor.constraintEqualToAnchor(leftAnchor),
            yearLabel.rightAnchor.constraintEqualToAnchor(rightAnchor),

        ])
    }
}



class Ring:UIView
{
    var fillColor: UIColor = UIColor.clearColor() {
        didSet {
            shapeLayer.fillColor = fillColor.CGColor
        }
    }
    var strokeColor: UIColor = UIColor.redColor() {
        didSet {
            shapeLayer.strokeColor = strokeColor.CGColor
        }
    }
    
    private var shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        return shapeLayer
    }()
    
    internal func drawRingFittingInsideView()->()
    {
        let halfSize:CGFloat = min( bounds.size.width/2, bounds.size.height/2)
        let desiredLineWidth:CGFloat = 1    // your desired value
        
        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x:halfSize,y:halfSize),
            radius: CGFloat( halfSize - (desiredLineWidth/2) ),
            startAngle: CGFloat(0),
            endAngle:CGFloat(M_PI * 2),
            clockwise: true)
        
        shapeLayer.path = circlePath.CGPath
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        shapeLayer.strokeColor = UIColor.redColor().CGColor
        shapeLayer.lineWidth = desiredLineWidth
        
        layer.anchorPoint = CGPointMake(0.5, 0.5)
        layer.position = CGPointMake(self.layer.bounds.midX, self.layer.bounds.midY)
        
        layer.addSublayer(shapeLayer)
    }
    
    override func layoutSubviews() {
        drawRingFittingInsideView()
    }
}
