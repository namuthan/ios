//
//  ViewController.swift
//  CustomCalenderView
//
//  Created by Nabil Muthanna on 2016-10-07.
//  Copyright © 2016 Nabil Muthanna. All rights reserved.
//

import UIKit

class CalenderViewController: UIViewController {

    
    private lazy var calenderView: CalendarView = {
        let view = CalendarView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -1, toDate: NSDate(), options: NSCalendarOptions.init(rawValue: 0))!

        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CalenderViewController.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
    
    }
    
    func rotated()
    {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            
        }
        
        calenderView.invalidateLayout()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewWillLayoutSubviews() {
        
        view.addSubview(calenderView)
        view.backgroundColor = .blackColor()
        
        NSLayoutConstraint.activateConstraints([
            //, constant: 20
            calenderView.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor),
            calenderView.leftAnchor.constraintEqualToAnchor(view.leftAnchor),
            calenderView.rightAnchor.constraintEqualToAnchor(view.rightAnchor),
            calenderView.bottomAnchor.constraintEqualToAnchor(view.bottomAnchor),

        ])
    }
    
    override func viewDidLayoutSubviews() {
        calenderView.scrollToCurrentMonth()
    }
}


class AppStyle: NSObject {
    
    static let MANIN_COLOR = UIColor(red: 255/255, green: 71/255, blue: 0.0, alpha: 1.0)
    static let BACKGROUND_COLOR = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
    
    static let CELL_CHECKED_ICON_STRING = "checkedIcon.png"
    
    static let DATE_FORMATTER: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()
    
    static let HEADER_FONT = UIFont.systemFontOfSize(12)
    static let DETAIL_FONT = UIFont.systemFontOfSize(14)
    static let MAIN_FONT = UIFont.systemFontOfSize(16)
}

